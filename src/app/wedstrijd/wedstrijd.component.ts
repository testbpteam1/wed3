import { Component, OnInit } from '@angular/core';

interface Uitslag {
  team1Score: number;
  team2Score: number;
}

interface Wedstrijd {
  club1: string;
  club2: string;
  uitslag: Uitslag;
}

@Component({
  selector: 'app-wedstrijd',
  templateUrl: './wedstrijd.component.html',
  styleUrls: ['./wedstrijd.component.css']
})
export class WedstrijdComponent implements OnInit {

  wedstrijd: Wedstrijd;
  wedstrijden: Wedstrijd[] = [];

  team1: string[] = ['ajax', 'feijnoord', 'psv', 'barcelona', 'real madrid', 'valencia', 'juventus'];
  team2: string[] = ['rodajc', 'helmond sport', 'sparta', 'mexico-city', 'club brugge', 'rode tijgers', 'apen'];

  team1T: string = '';
  team2T: string = '';

  ngOnInit() {

    for (let i = 0; i < 7; i++) {
      this.wedstrijden[i] = {
        club1: this.team1[i], club2: this.team2[i],
        'uitslag': { team1Score: Math.round(Math.random() * 2), team2Score: Math.round(Math.random() * 2) }
      }
    }

  }

  getCompetitieregel(i: number): Wedstrijd {
    return this.wedstrijden[i];
  }

  voegClubsToe() {
    this.wedstrijden.push({
      club1: this.team1T, club2: this.team2T,
      'uitslag': { team1Score: Math.round(Math.random() * 2), team2Score: Math.round(Math.random() * 2) }
    });

    this.team1T = '';
    this.team2T = '';
  }

  clubsIngevuld () : boolean {
    if(this.team1T.length > 0 && this.team2T.length > 0) {
      return true;
    }

      return false;
  }


}


